<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Role;
use App\Http\Controllers\RoleController;
use App\Http\Resources\RoleResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/roles', function(){
    return RoleResource::collection(Role::all());
});

Route::get('/role/{id}', function($id){
    return new RoleResource(Role::findOrFail($id));
});

Route::post('/roles', [RoleController::class, 'store']);

Route::put('/role/{id}', [RoleController::class, 'update']);

Route::delete('/role/{id}', [RoleController::class, 'destroy']);