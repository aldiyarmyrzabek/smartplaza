<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Role::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Role::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Role::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Role::where('id', $id)->exists()){
            $role = Role::find($id);
            $role->title = $request->title;
            $role->description = $request->description;
            $role->save();
            return response()->json([
                "message" => "Role updated successfully"
            ], 200);
        } else {
            return response()->json([
                "message" => "Role not found"
            ], 404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Role::where('id', $id)->exists()){
            $role = Role::find($id);
            $role->delete();
            return response()->json([
                "message" => "Role deleted"
            ], 200);
        } else {
            return response()->json([
                "message" => "Role not found"
            ], 404);
        }
        
    }
}
