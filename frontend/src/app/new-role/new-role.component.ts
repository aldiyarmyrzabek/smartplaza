import { Component, OnInit } from '@angular/core';
import { RoleService } from '../services/role.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-new-role',
  templateUrl: './new-role.component.html',
  styleUrls: ['./new-role.component.scss']
})
export class NewRoleComponent implements OnInit {

  constructor(private roleService: RoleService, private router:Router) { }
  roles: any;

  ngOnInit(): void {
  }

  add(roleTitle:string, roleDescription:string){
    this.roles = {
      'title': roleTitle,
      'description': roleDescription
    };
    this.roleService.addRole(this.roles as any).subscribe(role=>{
      this.roles = role
    });
    this.router.navigateByUrl('/');
  }
}
