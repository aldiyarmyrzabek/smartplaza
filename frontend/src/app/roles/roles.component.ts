import { Component, OnInit } from '@angular/core';
import { RoleService } from '../services/role.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private roleService: RoleService) { }

  roles: any;
  ngOnInit(): void {
    this.showRoles();
  }

  showRoles(){
    this.roles = this.roleService.listRoles().subscribe(role=>{
      this.roles = role;
    })
  }

  deleteRole(id:any){
    this.roleService.deleteRole(id).subscribe(
      res => {
        this.showRoles();
      }
    );
  }

}
