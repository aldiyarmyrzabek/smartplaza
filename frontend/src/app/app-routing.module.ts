import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditRoleComponent } from './edit-role/edit-role.component';
import { NewRoleComponent } from './new-role/new-role.component';

const routes: Routes = [
  {
    path: 'add', component: NewRoleComponent
  },
  {
    path: 'edit/:roleId', component: EditRoleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
}) 
export class AppRoutingModule { }
