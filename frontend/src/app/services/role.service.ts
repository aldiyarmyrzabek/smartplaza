import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RoleService {

  url:string = 'http://localhost:8000';

  constructor(private http: HttpClient) {

  }

  listRoles(){
    return this.http.get<any>(this.url+'/api/roles');
  }
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  
  addRole(role:any): Observable<any>{
    return this.http.post<any>(this.url+'/api/roles', role, this.httpOptions)
  }

  find(id:number): Observable<any> {
    return this.http.get(this.url + '/api/role/' + id)
  }

  update(id:number, role:any): Observable<any>{
    return this.http.put(this.url + '/api/role/' + id, role, this.httpOptions);
  }

  deleteRole(id:number): Observable<any>{
    return this.http.delete(this.url + '/api/role/' + id, this.httpOptions);
  }

}
