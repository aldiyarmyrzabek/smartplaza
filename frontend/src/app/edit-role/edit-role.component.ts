import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss']
})

export class EditRoleComponent implements OnInit {

  roleId: any;
  role: any;
  constructor(private route: ActivatedRoute, private router: Router, private roleService: RoleService) { }

  ngOnInit(): void {
    this.roleId = this.route.snapshot.params['roleId'];
    this.roleService.find(this.roleId).subscribe((data:any)=>{
      this.role = data;
    });
  }

  update(roleTitle:string, roleDescription:string){
    this.roleService.update(this.roleId, this.role).subscribe((res)=>{
      this.router.navigateByUrl('/');
    });
  }

}
